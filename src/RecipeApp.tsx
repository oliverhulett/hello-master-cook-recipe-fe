import { Redirect, Route, Switch } from 'react-router-dom'

import { PlusMenu } from './PlusMenu/PlusMenu'
import { RecipeSearchApp } from './RecipeSearch/RecipeSearchApp'

export const RecipeApp = () => {
  return (
    <>
      <Switch>
        <Redirect exact={true} from="/" to="/search" />
        <Route path="/search">
          <RecipeSearchApp />
        </Route>
      </Switch>
      {
        // Should float above the content and stick to the bottom right of the page.
        // The component itself shouldn't know it is doing this, that style has to come from here.
        // Or is that true?  Floating bottom right, menus will open to the left and up from the (+);
        // if it is used from somewhere else in a different position, would that change the internal logic of the component?
      }
      <PlusMenu />
    </>
  )
}
