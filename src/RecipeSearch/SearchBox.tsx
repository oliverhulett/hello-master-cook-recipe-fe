import React, { ChangeEvent, FormEvent, useState } from 'react'
import styled from 'styled-components'

import { Logo } from './Logo'

const Form = styled.form`
  margin: auto;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  max-height: 100%;
`

const LogoWrapper = styled.span`
  display: inline-block;
  width: 10%;
  max-height: 100%;
`

const Input = styled.input`
  width: 70%;
  margin: 1em;
`

const Button = styled.button`
  width: 10%;
`

export type SearchBoxProps = {
  query: string
  onSearch: (query: string) => void
}

export const SearchBox = ({ query, onSearch }: SearchBoxProps) => {
  const [searchString, setSearchString] = useState(query)

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchString(event.target.value)
    // TODO:  typeahead...
    event.preventDefault()
  }

  const handleSearch = (event: FormEvent<HTMLFormElement>) => {
    console.log('handleSearch : ' + searchString)
    if (searchString) {
      onSearch(searchString)
    }
    event.preventDefault()
  }

  return (
    <Form onSubmit={(event) => handleSearch(event)}>
      <LogoWrapper>
        <Logo />
      </LogoWrapper>
      <Input
        type="text"
        placeholder="Search for a recipe or ingredients" // TODO: Translatable
        value={searchString}
        onChange={(event) => handleChange(event)}
      />
      <Button type="submit">Search</Button>
    </Form>
  )
}
