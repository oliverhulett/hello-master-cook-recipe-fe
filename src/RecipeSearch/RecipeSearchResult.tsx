import React from 'react'

export type RecipeSearchResultProps = {
  key: string
}

export const RecipeSearchResult = ({ key }: RecipeSearchResultProps) => {
  return (
    <div>
      <p>Result: {key}</p>
    </div>
  )
}
