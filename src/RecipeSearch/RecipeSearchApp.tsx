import { useState } from 'react'
import styled from 'styled-components'
import { useHistory, useLocation } from 'react-router-dom'

import { SearchBox } from './SearchBox'
import {
  RecipeSearchResultsList,
  State as ResultListState,
} from './RecipeSearchResultsList'
import { RecipeSearchResultProps } from './RecipeSearchResult'

const SearchBoxSection = styled.section`
  margin: auto;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: height 1s, width 1s;
`

const SearchResultsSection = styled.section`
  align-items: center;
  justify-content: center;
`

// A custom hook that builds on useLocation to parse
// the query string for you.
function useQuery() {
  return new URLSearchParams(useLocation().search)
}

export const RecipeSearchApp = () => {
  const query = useQuery()
  const history = useHistory()
  const url = useLocation()

  //after submit form redirect user
  const [results, setResults] = useState<RecipeSearchResultProps[]>([])

  const handleSearch = (query: string) => {
    console.log('handleSearch : ' + query)
    history.push(url.pathname + '?q=' + query)
  }

  const queryStr = query.get('q')
  const resultsState = ResultListState.HIDDEN
  const style = queryStr
    ? {
        height: '2em',
        width: '90%',
      }
    : {}

  return (
    <>
      <SearchBoxSection style={style}>
        <SearchBox query={queryStr || ''} onSearch={handleSearch} />
      </SearchBoxSection>
      <SearchResultsSection>
        <RecipeSearchResultsList state={resultsState} results={results} />
      </SearchResultsSection>
    </>
  )
}
