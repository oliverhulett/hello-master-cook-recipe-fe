import React from 'react'
import {
  RecipeSearchResult,
  RecipeSearchResultProps,
} from './RecipeSearchResult'

export enum State {
  HIDDEN,
  LOADING,
  VISIBLE,
}

export type RecipeSearchResultListProps = {
  state: State
  results: RecipeSearchResultProps[]
}

export const RecipeSearchResultsList = ({
  state,
  results,
}: RecipeSearchResultListProps) => {
  let s: string = 'unknown'
  if (state === State.HIDDEN) {
    s = 'hidden'
  } else if (state === State.LOADING) {
    s = 'loading'
  } else if (state === State.VISIBLE) {
    s = 'visible'
  }
  const resultList = results.map((result) => {
    return <RecipeSearchResult key={result.key} />
  })
  return (
    <div>
      <p>Search Results: {s}</p>
      {resultList}
    </div>
  )
}
