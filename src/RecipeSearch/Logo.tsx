import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import styled from 'styled-components'

import logo from './logo.png'

const Img = styled.img`
  max-width: 100%;
  max-height: 100%;
  object-fit: contain;
`

export const Logo = () => {
  const url = useLocation()
  // TODO: Alt text should be translatable (and make it a prop?)
  return (
    <a href={url.pathname}>
      <Img src={logo} alt="Search" />
    </a>
  )
}
