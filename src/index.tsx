import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { RecipeApp } from './RecipeApp'
import { reportWebVitals } from './reportWebVitals'
import styled from 'styled-components'
import { BrowserRouter } from 'react-router-dom'

const ViewPortDiv = styled.div`
  width: 99vw;
  height: 98vh;
  overflow: auto;
`

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <ViewPortDiv>
        <RecipeApp />
      </ViewPortDiv>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
