import React from 'react'
import { ComponentStory, ComponentMeta } from '@storybook/react'

import { RecipeApp } from './RecipeApp'

export default {
  title: 'RecipeApp',
  component: RecipeApp,
} as ComponentMeta<typeof RecipeApp>

const Template: ComponentStory<typeof RecipeApp> = () => <RecipeApp />

export const Search = Template.bind({})

export const SearchResults = Template.bind({})
